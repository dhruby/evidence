var gulp         = require('gulp');
var sass         = require('gulp-ruby-sass');
var minifycss    = require('gulp-minify-css');
var rename       = require('gulp-rename');
var concat       = require('gulp-concat');
var watch        = require('gulp-watch');
var copy         = require('gulp-copy');
var browserSync  = require('browser-sync').create();

var path = {
	css: {
		src: 'www/sass',
		dst: 'www/css'
	},
	font: {
		dst: 'www/fonts'
	},
	js: {
		dst: 'www/js'
	}
};

gulp.task('scss', function()
{
	return sass(path.css.src, { style: 'expanded' })
		.pipe(concat('style.min.css'))
		.pipe(minifycss())
		.pipe(gulp.dest(path.css.dst))
		.pipe(browserSync.stream());
});

gulp.task('jquery', function()
{
	gulp.src('bower_components/jquery/dist/jquery.js')
			.pipe(copy(path.js.dst + '/jquery', {prefix: 3}));
});

gulp.task('bootstrap-js', function()
{
	gulp.src('bower_components/bootstrap-sass/assets/javascripts/bootstrap.js')
			.pipe(copy(path.js.dst + '/bootstrap', {prefix: 5}));
});

gulp.task('nette-ajax-js', function()
{
	gulp.src('bower_components/nette.ajax.js/nette.ajax.js')
			.pipe(copy(path.js.dst + '/ajax', {prefix: 4}));
});

gulp.task('bootstrap-css', function()
{
	gulp.src('bower_components/bootstrap-sass/assets/stylesheets/bootstrap/**')
		.pipe(copy(path.css.src + '/bootstrap/', {prefix: 5}));
	gulp.src('bower_components/bootstrap-sass/assets/fonts/bootstrap/**')
			.pipe(copy(path.font.dst + '/bootstrap', {prefix: 5}));
});

gulp.task('watch', function()
{
	browserSync.init({
		proxy: "iis.dev" // TODO: move to some config file
	});

	gulp.watch(path.css.src + '/**/*.scss', ['scss']);
	gulp.watch("app/**").on('change', browserSync.reload);
});