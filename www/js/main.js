$(function()
{
	$.nette.ext('bs-modal', {
		init: function () {
			this.ext('snippets', true).after(function ($el) {
				if (!$el.is('.modal')) {
					return;
				}
				$el.modal('show');
			});
		}
	});

	$.nette.init();

	$('.login-send').on('click', function (e)
	{
		var $el = $(this);
		var text = $el.val();

		$.nette.ajax({
			start: function() {
				$el.addClass('js-loading')
					.val('Zpracovávám...')
					.attr('disabled', 'disabled');
			},
			complete: function() {
				$el.removeClass('js-loading')
					.val(text)
					.removeAttr('disabled');
			}
		}, $el, e);
	});
});

$(document).ready(function() {
	$('.dataTables').DataTable({
		"bLengthChange": false,
		"bInfo": false,
		"pageLength": 10,
		"aoColumnDefs": [
			{ 'bSortable': false, 'aTargets': [ -1 ] }
		]
	});
});