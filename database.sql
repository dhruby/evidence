SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `building`;
CREATE TABLE `building` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `house_number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `building` (`id`, `street`, `house_number`, `city`, `country`) VALUES
(1,	'Metodějova',	'23',	'Brno',	'Czech republic'),
(2,	'Metodějova',	'24',	'Brno',	'Czech republic'),
(3,	'Božetěchova',	'11',	'Brno',	'Czech republic');

DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `department` (`id`, `name`) VALUES
(1,	'Humanitní oddělení'),
(2,	'Počítačové grafiky'),
(3,	'Počítačových sití'),
(4,	'Zahraniční');

DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `identification` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D338D583C54C8C93` (`type_id`),
  KEY `IDX_D338D58354177093` (`room_id`),
  KEY `IDX_D338D583A23B42D` (`manufacturer_id`),
  CONSTRAINT `FK_D338D58354177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `FK_D338D583A23B42D` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`id`),
  CONSTRAINT `FK_D338D583C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `equipment_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `equipment` (`id`, `type_id`, `room_id`, `manufacturer_id`, `date_add`, `identification`) VALUES
(1,	1,	1,	1,	'2015-12-06 20:54:59',	'4j0700'),
(2,	2,	2,	2,	'2015-12-06 20:56:00',	'2l5s5k'),
(3,	3,	3,	3,	'2015-12-06 20:56:33',	'kw1jmt'),
(4,	4,	4,	4,	'2015-12-06 20:56:56',	'cba4zg');

DROP TABLE IF EXISTS `equipment_type`;
CREATE TABLE `equipment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `equipment_type` (`id`, `name`) VALUES
(1,	'Table'),
(2,	'Chair'),
(3,	'Computer'),
(4,	'White board');

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `manufacturer` (`id`, `name`) VALUES
(1,	'Ikea'),
(2,	'Jysk'),
(3,	'Asus'),
(4,	'Lux');

DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inserted_by_id` int(11) DEFAULT NULL,
  `equipment_id` int(11) DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `inserted_at` datetime NOT NULL,
  `completed_at` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8EE434215C6EDA50` (`inserted_by_id`),
  KEY `IDX_8EE43421517FE9FE` (`equipment_id`),
  KEY `IDX_8EE43421F4BD7827` (`assigned_to_id`),
  CONSTRAINT `FK_8EE43421517FE9FE` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`),
  CONSTRAINT `FK_8EE434215C6EDA50` FOREIGN KEY (`inserted_by_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_8EE43421F4BD7827` FOREIGN KEY (`assigned_to_id`) REFERENCES `technician` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `repair` (`id`, `inserted_by_id`, `equipment_id`, `assigned_to_id`, `inserted_at`, `completed_at`, `status`, `note`) VALUES
(1,	3,	1,	2,	'2015-12-06 21:08:00',	NULL,	1,	'Bad leg'),
(2,	3,	3,	2,	'2015-12-06 21:08:55',	'2015-12-06 21:08:55',	3,	'Computer don´t want start'),
(3,	3,	2,	1,	'2015-12-06 22:09:16',	NULL,	2,	'Bad leg');

DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `building_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_729F519BAE80F5DF` (`department_id`),
  KEY `IDX_729F519B4D2A7E12` (`building_id`),
  CONSTRAINT `FK_729F519B4D2A7E12` FOREIGN KEY (`building_id`) REFERENCES `building` (`id`),
  CONSTRAINT `FK_729F519BAE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `room` (`id`, `department_id`, `building_id`, `name`) VALUES
(1,	1,	2,	'A105'),
(2,	1,	1,	'A105'),
(3,	1,	3,	'A105'),
(4,	2,	1,	'A106'),
(5,	2,	2,	'A106'),
(6,	3,	1,	'A107'),
(7,	3,	2,	'A107');

DROP TABLE IF EXISTS `technician`;
CREATE TABLE `technician` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `leading` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F244E948A76ED395` (`user_id`),
  CONSTRAINT `FK_F244E948A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `technician` (`id`, `user_id`, `leading`) VALUES
(1,	1,	1),
(2,	2,	0);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `pin` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `IDX_8D93D64954177093` (`room_id`),
  CONSTRAINT `FK_8D93D64954177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `room_id`, `login`, `firstname`, `surname`, `password`, `pin`) VALUES
(1,	2,	'administrator',	'Petr',	'Novák',	'$2y$10$vSm3AfzHxzFehvO7M8/jkeN45Tk.1sXC9kiSrhLjput.T29XxWBvi',	'9402156874'),
(2,	6,	'technician',	'Jan',	'Nováček',	'$2y$10$SYBnCXka.PlVn3xK0ovbAuqoEJgyGhOK9k2QPkvVW82C00icyYEYK',	'8704125648'),
(3,	5,	'user0000',	'Martin',	'Pelánek',	'$2y$10$ItdzNe7Ynk5IRSNloF2bie/3/BW.A2z2Fle6NIuVQYgB3Va61omia',	'7785495214');

DROP TABLE IF EXISTS `user_equipment`;
CREATE TABLE `user_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B717074F517FE9FE` (`equipment_id`),
  KEY `IDX_B717074FA76ED395` (`user_id`),
  CONSTRAINT `FK_B717074F517FE9FE` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`),
  CONSTRAINT `FK_B717074FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user_equipment` (`id`, `equipment_id`, `user_id`) VALUES
(1,	1,	1),
(2,	2,	2),
(3,	3,	3),
(4,	4,	1);

DELIMITER $$
CREATE TRIGGER `pin_validate` BEFORE INSERT ON `user`
  FOR EACH ROW BEGIN
    IF NEW.pin NOT REGEXP '^[0-9]{6}/[0-9]{4}$' THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid pin';
    END IF;
  END$$
DELIMITER ;
-- 2015-12-06 20:12:59