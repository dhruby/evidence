<?php

namespace App\Model;

use Nextras\Dbal\Utils\DateTime;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Class User
 *
 * @property string $street
 * @property string $houseNumber
 * @property string $city
 * @property string $country
 * @property OneHasMany|Room[] $rooms {1:m Room::$building}
 */
class Building extends Entity
{

}