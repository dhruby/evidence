<?php

namespace App\Model;

use App\Model\Entity\User;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use Nextras\Orm\Entity\IEntity;

/**
 * Class UserManager
 */
class UserManager implements IAuthenticator
{

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * UserManager constructor.
	 * @param Orm $orm
	 */
	public function __construct(Orm $orm)
	{
		$this->orm = $orm;
	}

	/**
	 * @param array $credentials
	 * @return Identity
	 * @throws AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($login, $password) = $credentials;

		$user = $this->orm->user->getBy([
			'login' => $login
		]);

		if ($user == false)
		{
			throw new AuthenticationException('Invalid username');
		}
		elseif (!Passwords::verify($password, $user->password))
		{
			throw new AuthenticationException('Invalid username');
		}

		$arr = $user->toArray(IEntity::TO_ARRAY_RELATIONSHIP_AS_ID);
		unset($arr['password']);

		$roles = [];

		if ($user->hasValue('technician'))
		{
			$roles[] = 'technician';

			if($user->technician->leading)
			{
				$roles[] = 'admin';
			}
		}
		else
		{
			$roles[] = 'user';
		}

		return new Identity($user->id, $roles, $arr);
	}


	public function add(User $user)
	{
		$this->em->persist($user);
		$this->em->flush();
	}

}