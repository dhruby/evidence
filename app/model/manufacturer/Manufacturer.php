<?php

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Class User
 *
 * @property string $name
 * @property OneHasMany|Equipment[] $equipments {1:m Equipment::$manufacturer}
 */
class Manufacturer extends Entity
{

}