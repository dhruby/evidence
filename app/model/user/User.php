<?php

namespace App\Model;

use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Entity\Entity;

/**
 * Class User
 *
 * @property string $login
 * @property string $firstname
 * @property string $surname
 * @property string $password
 * @property string $pin
 * @property OneHasMany|UserEquipment[] $equipments   {1:m UserEquipment::$user}
 * @property Technician|NULL $technician           {1:1d Technician::$user}
 * @property Repair[] $repairs                     {1:m Repair::$insertedBy}
 * @property Room $room                            {m:1 Room::$users}
 */
class User extends Entity
{

}