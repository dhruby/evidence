<?php

namespace App\Model;

use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\ManyHasOne;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Class Room
 *
 * @property string $name
 * @property OneHasMany|Equipment[] $equipments {1:m Equipment::$room}
 * @property ManyHasOne|Building $building {m:1 Building::$rooms}
 * @property ManyHasOne|Department $department {m:1 Department::$rooms}
 * @property OneHasMany|Room $users {1:m User::$room}
 */
class Room extends Entity
{
	public function canBeDeleted($user)
	{
		return $user->isAllowed('Room', 'delete');
	}
}