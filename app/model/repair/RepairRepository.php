<?php

namespace App\Model;

use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Repository\IDependencyProvider;
use Nextras\Orm\Repository\Repository;

/**
 * @method Repair|NULL getById($id)
 */
class RepairRepository extends Repository
{

	/**
	 * RepairRepository constructor.
	 * @param IMapper $mapper
	 * @param IDependencyProvider|NULL $dependencyProvider
	 */
	public function __construct(IMapper $mapper, IDependencyProvider $dependencyProvider = NULL)
	{
		parent::__construct($mapper, $dependencyProvider);

		$this->onBeforeInsert[] = function (Repair $repair)
		{
			$repair->insertedAt = 'now';
			$repair->status = $repair::STATUS_NEW;
		};
	}
}