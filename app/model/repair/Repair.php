<?php

namespace App\Model;

use Nextras\Dbal\Utils\DateTime;
use Nextras\Orm\Entity\Entity;

/**
 * Class User
 *
 * @property DateTime $insertedAt
 * @property DateTime|NULL $completedAt
 * @property string|NULL $note
 * @property int $status {enum self::STATUS_*}
 * @property User $insertedBy {m:1 User::$repairs}
 * @property Technician|NULL $assignedTo {m:1 Technician::$repairs}
 * @property Equipment $equipment {m:1 Equipment::$repairs}
 *
 * @property-read string $statusText {virtual}
 */
class Repair extends Entity
{

	const STATUS_NEW       = 1;
	const STATUS_REPAIRING = 2;
	const STATUS_FINISHED  = 3;

	public function canBeEdited($user)
	{
		return $user->isAllowed('Repair', 'edit');
	}

	public function canBeDeleted($user)
	{
		return $user->isAllowed('Repair', 'deleted');
	}

	public function canBeCompleted($user)
	{
		return is_null($this->completedAt) && $user->isAllowed('Repair', 'complete');;
	}

	public function canBeAssigned($user)
	{
		return $user->isAllowed('Repair', 'assigned');
	}

	public function getterStatusText()
	{
		$text = [
			self::STATUS_NEW       => "New",
			self::STATUS_REPAIRING => "Repairing",
			self::STATUS_FINISHED  => "Finished"
		];

		return $text[$this->status];
	}
}