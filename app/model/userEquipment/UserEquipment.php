<?php

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Class UserEquipment
 *
 * @property User $user {m:1 User::$equipments}
 * @property Equipment $equipment  {m:1 Equipment::$users}
 */
class UserEquipment extends Entity
{

}