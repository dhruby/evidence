<?php

namespace App\Model;

use Nette\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\ManyHasOne;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Class User
 *
 * @property EquipmentType $type            {m:1 EquipmentType::$equipments}
 * @property Room $room                     {m:1 Room::$equipments}
 * @property Manufacturer $manufacturer     {m:1 Manufacturer::$equipments}
 * @property Repair[] $repairs              {1:m Repair::$equipment}
 * @property DateTime $dateAdd
 * @property string $identification
 * @property UserEquipment[] $users   {1:m UserEquipment::$equipment}
 */
class Equipment extends Entity
{

}