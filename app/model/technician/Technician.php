<?php

namespace App\Model;

use Nextras\Orm\Entity\Entity;

/**
 * Class User
 *
 * @property string $leading
 * @property User $user        {1:1d User::$technician primary}
 * @property Repair[] $repairs {1:m Repair::$assignedTo}
 */
class Technician extends Entity
{

}