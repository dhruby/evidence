<?php

namespace App\Model;

use Nextras\Orm\Model\Model;

/**
 * Model
 *
 * @property-read BuildingRepository     $building
 * @property-read DepartmentRepository   $department
 * @property-read EquipmentRepository    $equipment
 * @property-read EquipmentTypeRepository    $equipmentType
 * @property-read ManufacturerRepository $manufacturer
 * @property-read RoomRepository         $room
 * @property-read RepairRepository       $repair
 * @property-read TechnicianRepository   $technician
 * @property-read UserRepository         $user
 * @property-read UserEquipmentRepository    $userEquipment
 */
class Orm extends Model
{
}