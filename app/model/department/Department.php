<?php

namespace App\Model;

use Nette\Utils\DateTime;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\ManyHasOne;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Class User
 *
 * @property string $name
 * @property OneHasMany|Room[] $rooms {1:m Room::$department}
 */
class Department extends Entity
{

}