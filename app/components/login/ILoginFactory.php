<?php

namespace App\Components\Login;

/**
 * Interface ILoginFactory
 */
interface ILoginFactory
{

	/**
	 * @return LoginControl
	 */
	public function create();

}