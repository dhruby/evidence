<?php

namespace App\Components\Login;

use App\Model\Orm;
use Nette\Application\UI;
use Nette\Security\AuthenticationException;
use Nette\Security\User as NSUser;
use Nextras\Forms\Rendering\Bs3FormRenderer;

/**
 * Class LoginControl
 */
class LoginControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];
	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var NSUser
	 */
	private $user;

	/**
	 * LoginControl constructor.
	 * @param Orm $orm
	 * @param NSUser $user
	 */
	public function __construct(Orm $orm, NSUser $user)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->user = $user;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/login.latte');
		$this->template->form = $this['login'];
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentLogin()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('login', 'Jméno')
			->setRequired('Zadejte jméno.');
		$form->addPassword('password', 'Heslo')
			->setRequired('Zadejte heslo.');
		$form->addCheckbox('remember', 'Zapamatovat přihlášení');
		$form->addSubmit('send', 'Přihlásit se')->setAttribute('class', 'login-send');

		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		try
		{
			$this->user->login($values->login, $values->password);

			if ($values->remember)
			{
				$this->user->setExpiration('+14 day');
			}

			$this->onSuccess();
		}
		catch (AuthenticationException $e)
		{
			$form->addError('Neplatné údaje');
			if ($this->presenter->isAjax())
			{
				$this->redrawControl();
			}
		}

	}

}