<?php

namespace App\Components\Room;

use App\Model\Orm;
use App\Model\Room;
use Nette\Application\UI;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class RoomControl
 */
class RoomControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * RoomControl constructor.
	 * @param int|null $id
	 * @param Orm $em
	 */
	public function __construct($id, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$room = $this->orm->room->getById($this->id);

			$this['room']['name']->setValue($room->name);
			$this['room']['building']->setValue($room->building->id);
			$this['room']['department']->setValue($room->department->id);
		}

		$this->template->setFile(__DIR__ . '/room.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentRoom()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('name', 'Name:');
		$form->addSelect('building', 'Building:', $this->getBuildings());
		$form->addSelect('department', 'Department:', $this->orm->department->findAll()->fetchPairs('id', 'name'));
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		if(is_null($this->id))
		{
			$room = new Room();
			$this->presenter->flashMessage('Room was successfully saved.');
		}
		else
		{
			$room = $this->orm->room->getById($this->id);
			$this->presenter->flashMessage("Room was successfully changed.");
		}

		$room->name = $values['name'];
		$room->building = $this->orm->building->getById($values['building']);
		$room->department = $this->orm->department->getById($values['department']);

		$this->orm->room->persistAndFlush($room);

		$this->onSuccess();
	}

	/**
	 * @return array
	 */
	protected function getBuildings()
	{
		$buildings = array();
		foreach($this->orm->building->findAll() as $building)
		{
			$buildings[$building->id] = $building->country ." ". $building->city." ". $building->street . " ". $building->houseNumber;
		}

		return $buildings;
	}

}