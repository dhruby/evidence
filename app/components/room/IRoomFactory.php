<?php

namespace App\Components\Room;

/**
 * Interface IRoomFactory
 */
interface IRoomFactory
{

	/**
	 * @return RoomControl
	 */
	public function create($id);

}