<?php

namespace App\Components\Repair;

/**
 * Interface IRepairFactory
 */
interface IRepairFactory
{

	/**
	 * @param $id
	 * @return RepairControl
	 */
	public function create($id);

}