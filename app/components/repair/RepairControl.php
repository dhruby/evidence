<?php

namespace App\Components\Repair;

use App\Model\Orm;
use App\Model\Repair;
use Nette;
use Nette\Application\UI;
use Nette\Utils\DateTime;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class RepairControl
 */
class RepairControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var Nette\Security\User
	 */
	private $user;

	/**
	 * RepairControl constructor.
	 * @param \Nette\ComponentModel\IContainer $id
	 * @param Nette\Security\User $user
	 * @param Orm $orm
	 */
	public function __construct($id, Nette\Security\User $user, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
		$this->user = $user;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$repair = $this->orm->repair->getById($this->id);

			//$this['repair']['assignedTo']->setValue($repair->assignedTo->id);
			$this['repair']['equipment']->setValue($repair->equipment->id);
			$this['repair']['note']->setValue($repair->note);
		}

		$this->template->setFile(__DIR__ . '/repair.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentRepair()
	{
		$users = [];
		foreach ($this->orm->technician->findAll() as $technician)
		{
			$users[$technician->id] = $technician->user->login;
		}

		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		//$form->addSelect('assignedTo', 'Assigned to:', $users);
		$form->addSelect('equipment', 'Equipment:', $this->orm->equipment->findAll()->fetchPairs('id', 'identification'));
		$form->addTextArea('note', 'Note:');
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		if(is_null($this->id))
		{
			$repair = new Repair;
			$this->presenter->flashMessage("Repair was successfully saved.");
		}
		else
		{
			$repair = $this->orm->repair->getById($this->id);
			$this->presenter->flashMessage("Repair was successfully changed.");
		}

		//$repair->assignedTo = $this->orm->technician->getById($values['assignedTo']);
		$repair->insertedBy = $this->orm->user->getById($this->user->id);
		$repair->equipment = $this->orm->equipment->getById($values['equipment']);
		$repair->insertedAt = new DateTime();
		$repair->status = 1;
		$repair->note = $values['note'];

		$this->orm->repair->persistAndFlush($repair);

		$this->onSuccess();
	}

}