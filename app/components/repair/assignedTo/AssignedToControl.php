<?php

namespace App\Components\Repair\AssignedTo;

use App\Model\Orm;
use App\Model\Repair;
use Nette;
use Nette\Application\UI;
use Nette\Utils\DateTime;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class AssignedToControl
 */
class AssignedToControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var Nette\Security\User
	 */
	private $user;

	/**
	 * AssignedToControl constructor.
	 * @param \Nette\ComponentModel\IContainer $id
	 * @param Nette\Security\User $user
	 * @param Orm $orm
	 */
	public function __construct($id, Nette\Security\User $user, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
		$this->user = $user;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$repair = $this->orm->repair->getById($this->id);

			if($repair->assignedTo)
			{
				$this['assignedTo']['assignedTo']->setValue($repair->assignedTo->id);
			}
		}

		$this->template->setFile(__DIR__ . '/assignedTo.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentAssignedTo()
	{
		$users = [];
		foreach ($this->orm->technician->findAll() as $technician)
		{
			$users[$technician->id] = $technician->user->login;
		}

		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addSelect('assignedTo', 'Assigned to:', $users);
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		$repair = $this->orm->repair->getById($this->id);
		$this->presenter->flashMessage("Technician was successfully assigned to repair.");

		$repair->assignedTo = $this->orm->technician->getById($values['assignedTo']);

		$this->orm->repair->persistAndFlush($repair);

		$this->onSuccess();
	}

}