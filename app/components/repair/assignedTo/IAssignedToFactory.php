<?php

namespace App\Components\Repair\AssignedTo;

/**
 * Interface IAssignedToFactory
 */
interface IAssignedToFactory
{

	/**
	 * @param $id
	 * @return AssignedToControl
	 */
	public function create($id);

}