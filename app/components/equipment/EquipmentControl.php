<?php

namespace App\Components\Equipment;

use App\Model\Equipment;
use App\Model\Orm;
use App\Model\UserEquipment;
use Nette;
use Nette\Application\UI;
use Nette\Utils\DateTime;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class EquipmentControl
 */
class EquipmentControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var Nette\Security\User
	 */
	private $user;

	/**
	 * EquipmentControl constructor.
	 * @param int|null $id
	 * @param Orm $orm
	 * @param Nette\Security\User $user
	 */
	public function __construct($id, Orm $orm,  Nette\Security\User $user)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
		$this->user = $user;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$equipment = $this->orm->equipment->getById($this->id);

			$this['equipment']['identification']->setValue($equipment->identification);
			$this['equipment']['type']->setValue($equipment->type->id);
			$this['equipment']['room']->setValue($equipment->room->id);
			$this['equipment']['manufacturer']->setValue($equipment->manufacturer->id);
        }

		$this->template->setFile(__DIR__ . '/equipment.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentEquipment()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('identification', 'Identification:')
            ->addRule(UI\Form::MAX_LENGTH, 'Poznámka je příliš dlouhá', 15);
		$form->addSelect('type', 'Type:', $this->orm->equipmentType->findAll()->fetchPairs('id', 'name'));
		$form->addSelect('room', 'Room:', $this->orm->room->findAll()->fetchPairs('id', 'name'));
		$form->addSelect('manufacturer', 'Manufacturer:', $this->orm->manufacturer->findAll()->fetchPairs('id', 'name'));
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		if(is_null($this->id))
		{
			$equipment = new Equipment();
			$this->presenter->flashMessage("Equipment was successfully saved.");

            $userEquipment = new UserEquipment();
            $userEquipment->user = $this->orm->user->getById($this->user->id);
            $userEquipment->equipment = $equipment;

		}
		else
		{
			$equipment = $this->orm->equipment->getById($this->id);
			$this->presenter->flashMessage("Equipment was successfully changed.");
		}

		$equipment->identification = $values['identification'];
		$equipment->type = $this->orm->equipmentType->getById($values['type']);
		$equipment->room = $this->orm->room->getById($values['room']);
		$equipment->manufacturer = $this->orm->manufacturer->getById($values['manufacturer']);
		$equipment->dateAdd = new DateTime();

		$this->orm->equipment->persistAndFlush($equipment);

        if(is_null($this->id))
        {
            $this->orm->userEquipment->persistAndFlush($userEquipment);
        }

		$this->onSuccess();
	}

}