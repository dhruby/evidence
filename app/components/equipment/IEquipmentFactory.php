<?php

namespace App\Components\Equipment;

/**
 * Interface IEquipmentFactory
 */
interface IEquipmentFactory
{

	/**
	 * @return EquipmentControl
	 */
	public function create($id);

}