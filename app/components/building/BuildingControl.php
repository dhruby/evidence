<?php

namespace App\Components\Building;

use App\Model\Building;
use App\Model\Orm;
use Nette\Application\UI;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class BuildingControl
 */
class BuildingControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * BuildingControl constructor.
	 * @param int|null $id
	 * @param Orm $orm
	 */
	public function __construct($id, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$building = $this->orm->building->getById($this->id);

			$this['building']['street']->setValue($building->street);
			$this['building']['houseNumber']->setValue($building->houseNumber);
			$this['building']['city']->setValue($building->city);
			$this['building']['country']->setValue($building->country);
		}

		$this->template->setFile(__DIR__ . '/building.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentBuilding()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('street', 'Street:*')
			->setRequired();
		$form->addText('houseNumber', 'House number:*')
			->addRule($form::NUMERIC, 'Údaj musí být numerický')
			->setRequired();
		$form->addText('city', 'City:*	')
			->setRequired();;
		$form->addText('country', 'Country:');
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		if(is_null($this->id))
		{
			$building = new Building();
			$this->presenter->flashMessage("Buiding was successfully saved.");
		}
		else
		{
			$building = $this->orm->building->getById($this->id);
			$this->presenter->flashMessage("Buiding was successfully changed.");
		}

        $building->street = $values['street'];
        $building->houseNumber = $values['houseNumber'];
        $building->city = $values['city'];
        $building->country = $values['country'];

		$this->orm->building->persistAndFlush($building);

		$this->onSuccess();
	}

}