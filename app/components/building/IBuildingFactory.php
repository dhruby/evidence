<?php

namespace App\Components\Building;

/**
 * Interface IBuildingFactory
 */
interface IBuildingFactory
{

	/**
	 * @return BuildingControl
	 */
	public function create($id);

}