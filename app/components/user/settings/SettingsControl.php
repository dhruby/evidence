<?php

namespace App\Components\User\Settings;

use App\Model\User;
use App\Model\Room;
use App\Model\Orm;
use Nette\Application\UI;
use Nette\Security\Passwords;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class UserControl
 */
class SettingsControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * UserControl constructor.
	 * @param int|null $id
	 * @param Orm $orm
	 */
	public function __construct($id, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$user = $this->orm->user->getById($this->id);

			$this['user']['firstname']->setValue($user->firstname);
			$this['user']['surname']->setValue($user->surname);
			$this['user']['pin']->setValue($user->pin);
			$this['user']['room']->setValue($user->room->id);
		}

		$this->template->setFile(__DIR__ . '/user.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentUser()
	{
		$rooms = $this->orm->room->findAll()->fetchPairs('id', 'name');

		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('firstname', 'First name');
		$form->addText('surname', 'Surname');
		$form->addText('pin', 'Pin');
		$form->addSelect('room', 'Room', $rooms);
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		$user = $this->orm->user->getById($this->id);

		$user->firstname = $values['firstname'];
		$user->surname = $values['surname'];
		$user->surname = $values['surname'];
		$user->pin = $values['pin'];
		$user->room = $this->orm->room->getById($values['room']);

		$this->orm->user->persistAndFlush($user);

		$this->presenter->flashMessage('Settings was successfully changed.');

		$this->onSuccess();
	}

}