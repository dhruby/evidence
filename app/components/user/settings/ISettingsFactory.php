<?php

namespace App\Components\User\Settings;

/**
 * Interface IUserFactory
 * @package App\Components\User\Settings
 */
interface ISettingsFactory
{

	/**
	 * @param int $id
	 * @return SettingsControl
	 */
	public function create($id);

}