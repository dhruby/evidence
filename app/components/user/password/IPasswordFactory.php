<?php

namespace App\Components\User\Password;

/**
 * Interface IUserFactory
 * @package App\Components\User\Password
 */
interface IPasswordFactory
{

	/**
	 * @param int $id
	 * @return PasswordControl
	 */
	public function create($id);

}