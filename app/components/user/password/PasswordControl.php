<?php

namespace App\Components\User\Password;

use App\Model\User;
use App\Model\Room;
use App\Model\Orm;
use Nette\Application\UI;
use Nette\Security\Passwords;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class UserControl
 */
class PasswordControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * UserControl constructor.
	 * @param int|null $id
	 * @param Orm $orm
	 */
	public function __construct($id, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/password.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentUser()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addPassword('currentPassword', 'Current password');
		$form->addPassword('newPassword', 'New password');
		$form->addPassword('confirmPassword', 'Confirm password');

		$form->addSubmit('save', 'Change password');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		$user = $this->orm->user->getById($this->id);

		if(!Passwords::verify($values['currentPassword'], $user->password))
		{
			$form->addError('Bad current password.');
			return;
		}

		if($values['newPassword'] != $values['confirmPassword'])
		{
			$form->addError('Passwords are not identical.');
			return;
		}

		$user->password = Passwords::hash($values['newPassword']);
		$this->orm->user->persistAndFlush($user);

		$this->presenter->flashMessage('Password was successfully changed.');

		$this->onSuccess();
	}

}