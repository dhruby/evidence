<?php

namespace App\Components\User;

/**
 * Interface IUserFactory
 */
interface IUserFactory
{

	/**
	 * @return UserControl
	 */
	public function create($id);

}