<?php

namespace App\Components\User;

use App\Model\User;
use App\Model\Room;
use App\Model\Orm;
use Nette\Application\UI;
use Nextras\Forms\Rendering\Bs3FormRenderer;
use Nextras\Dbal\QueryException;
use Nextras\Dbal\UniqueConstraintViolationException;


/**
 * Class UserControl
 */
class UserControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * UserControl constructor.
	 * @param int|null $id
	 * @param Orm $orm
	 */
	public function __construct($id, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$user = $this->orm->user->getById($this->id);

			$this['user']['login']->setValue($user->login);
			$this['user']['firstname']->setValue($user->firstname);
			$this['user']['surname']->setValue($user->surname);
			$this['user']['password']->setValue($user->password);
			$this['user']['pin']->setValue($user->pin);
			$this['user']['room']->setValue($user->room->id);
		}

		$this->template->setFile(__DIR__ . '/user.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentUser()
	{
		$rooms = $this->orm->room->findAll()->fetchPairs('id', 'name');

		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('login', 'Login:');
		$form->addText('firstname', 'First name:');
		$form->addText('surname', 'Surname:');
		$form->addPassword('password', 'Password:');
		$form->addText('pin', 'Pin:');
		$form->addSelect('room', 'Room:', $rooms);
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		if(is_null($this->id))
		{
			$user = new User();
			$this->flashMessage("User was successfully saved.");
		}
		else
		{
			$user = $this->orm->user->getById($this->id);
			$this->flashMessage("User was successfully changed.");
		}

		try
		{
			$user->login = $values['login'];
			$user->firstname = $values['firstname'];
			$user->surname = $values['surname'];
			$user->password = $values['password'];
			$user->surname = $values['surname'];
			$user->pin = $values['pin'];
			$user->room = $this->orm->room->getById($values['room']);

			$this->orm->user->persistAndFlush($user);

			$this->onSuccess();
		}
		catch (UniqueConstraintViolationException $e)
		{
			$form['login']->addError('Username is already in use');
		}
		catch (QueryException $e)
		{
			if ($e->getMessage() == "Invalid pin")
			{
				$form['pin']->addError('Pin must be in format 999999/9999');
			}
		}
	}

}