<?php

namespace App\Components\Registration;

use App\Model\Orm;
use App\Model\User;
use Nette\Application\UI;
use Nette\Security\Passwords;
use Nette\Security\User as NSUser;
use Nextras\Dbal\QueryException;
use Nextras\Forms\Rendering\Bs3FormRenderer;
use Nextras\Dbal\UniqueConstraintViolationException;


/**
 * Class UserControl
 */
class RegistrationControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var NSUser
	 */
	private $user;


	/**
	 * UserControl constructor.
	 * @param int|null $id
	 * @param NSUser $user
	 * @param Orm $orm
	 */
	public function __construct(Orm $orm, NSUser $user)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->user = $user;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/user.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentForm()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('login', 'Login:')
			->addRule($form::MIN_LENGTH, 'Login musí mít nejméně %d znaků', 8);
		$form->addText('firstname', 'First name:');
		$form->addText('surname', 'Surname:');
		$form->addPassword('password', 'Password:')
			->addRule($form::MIN_LENGTH, 'Heslo musí mít nejméně %d znaků', 5);
		$form->addText('pin', 'Pin:');
		$form->addSelect('room', 'Room:', $this->orm->room->findAll()->fetchPairs('id', 'name'));

		$form->addSubmit('save', 'Create account');

		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		try
		{
			$user = new User;

			$user->login = $values['login'];
			$user->firstname = $values['firstname'];
			$user->surname = $values['surname'];
			$user->password = Passwords::hash($values['password']);
			$user->surname = $values['surname'];
			$user->pin = $values['pin'];
			$user->room = $this->orm->room->getById($values['room']);

			$this->orm->user->persistAndFlush($user);

			$this->user->login($values->login, $values->password);

			$this->presenter->flashMessage("User was successfully saved.");

			$this->onSuccess();
		}
		catch (UniqueConstraintViolationException $e)
		{
			$form['login']->addError('Username is already in use');
		}
		catch (QueryException $e)
		{
			if ($e->getMessage() == "Invalid pin")
			{
				$form['pin']->addError('Pin must be in format 999999/9999');
			}
		}
	}

}