<?php

namespace App\Components\Registration;


interface IRegistrationFactory
{

	/**
	 * @return RegistrationControl
	 */
	public function create();

}