<?php

namespace App\Components\Department;

use App\Model\Department;
use App\Model\Orm;
use Nette\Application\UI;
use Nextras\Forms\Rendering\Bs3FormRenderer;


/**
 * Class DepartmentControl
 */
class DepartmentControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var Orm
	 */
	private $orm;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * BuildingControl constructor.
	 * @param int|null $id
	 * @param Orm $orm
	 */
	public function __construct($id, Orm $orm)
	{
		parent::__construct();
		$this->orm = $orm;
		$this->id = $id;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$department = $this->orm->department->getById($this->id);

			$this['department']['name']->setValue($department->name);
		}

		$this->template->setFile(__DIR__ . '/department.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentDepartment()
	{
		$form = new UI\Form;
		$form->setRenderer(new Bs3FormRenderer());
		$form->addText('name', 'Name:')
            ->setRequired();
		$form->addSubmit('save', 'Save');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		if (is_null($this->id))
		{
			$department = new Department();
			$this->presenter->flashMessage("Department was successfully saved.");
		}
		else
		{
			$department = $this->orm->department->getById($this->id);
			$this->presenter->flashMessage("Department was successfully changed.");
		}

		$department->name = $values['name'];

		$this->orm->department->persistAndFlush($department);

		$this->onSuccess();
	}

}