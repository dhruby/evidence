<?php

namespace App\Components\Department;

/**
 * Interface IDepartmentFactory
 */
interface IDepartmentFactory
{

	/**
	 * @return DepartmentControl
	 */
	public function create($id);

}