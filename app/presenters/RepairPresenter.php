<?php

namespace App\Presenters;

use App\Components\Repair\AssignedTo\AssignedToControl;
use App\Components\Repair\AssignedTo\IAssignedToFactory;
use App\Components\Repair\IRepairFactory;
use App\Components\Repair\RepairControl;
use Nette\Utils\DateTime;
use Nextras\Orm\NullValueException;

/**
 * Class RepairPresenter
 */
class RepairPresenter extends SecuredPresenter
{

	public function actionDelete($id)
	{
		$repair = $this->orm->repair->getById($id);
		try{
			$this->orm->repair->removeAndFlush($repair);
			$this->flashMessage("Repair was successfully deleted.");
		}catch (NullValueException $e){
			$this->flashMessage("Repair cant be deleted.", "warning");
		}

		$this->redirect('Repair:default');
	}

	public function handleComplete($id)
	{
		$repair = $this->orm->repair->getById($id);
		$repair->completedAt = new DateTime();
		$this->orm->repair->persistAndFlush($repair);
		$this->flashMessage("Repair was completed.");
		$this->redirect('Repair:default');
	}

	public function renderDefault($id = NULL)
	{
		$this->template->repairs = $this->orm->repair->findAll();
	}

	public function renderDetail($id)
	{
		$this->template->repair = $this->orm->repair->getById($id);
	}

	public function handleEdit($id)
	{
		$this->template->showAssignedTo = true;
		$this->redrawControl('edit');
	}

	public function renderEdit($id)
	{
	}

	public function renderAdd($id)
	{
	}

	/**
	 * @param IRepairFactory $repairFactory
	 * @return RepairControl
	 */
	protected function createComponentRepair(IRepairFactory $repairFactory)
	{
		$repair = $repairFactory->create($this->getParameter('id'));
		$repair->onSuccess[] = function ()
		{
			$this->redirect('Repair:default');
		};
		return $repair;
	}

	/**
	 * @param IAssignedToFactory $assignedToFactory
	 * @return AssignedToControl
	 */
	protected function createComponentAssignedTo(IAssignedToFactory $assignedToFactory)
	{
		$assignedTo = $assignedToFactory->create($this->getParameter('id'));
		$assignedTo->onSuccess[] = function ()
		{
			$this->redirect('Repair:default');
		};
		return $assignedTo;
	}

}
