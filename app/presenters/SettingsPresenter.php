<?php

namespace App\Presenters;

use App\Components\User\Password\IPasswordFactory;
use App\Components\User\Settings\ISettingsFactory;


class SettingsPresenter extends SecuredPresenter
{

	/**
	 * @param ISettingsFactory $settingsFactory
	 * @return \App\Components\User\Settings\SettingsControl
	 */
	protected function createComponentSettings(ISettingsFactory $settingsFactory)
	{
		$room = $settingsFactory->create($this->user->id);
		$room->onSuccess[] = function ()
		{
			$this->redirect('Settings:default');
		};
		return $room;
	}

	/**
	 * @param IPasswordFactory $passwordFactory
	 * @return \App\Components\User\Password\PasswordControl
	 */
	protected function createComponentPassword(IPasswordFactory $passwordFactory)
	{
		$room = $passwordFactory->create($this->user->id);
		$room->onSuccess[] = function ()
		{
			$this->redirect('Settings:default');
		};
		return $room;
	}
}
