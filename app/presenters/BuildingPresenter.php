<?php

namespace App\Presenters;

use App\Components\Building\BuildingControl;
use App\Components\Building\IBuildingFactory;
use Nette\Application\BadRequestException;
use Nextras\Orm\NullValueException;

/**
 * Class BuildingPresenter
 */
class BuildingPresenter extends SecuredPresenter
{

	public function handleDelete($id)
	{
		$building = $this->orm->building->getById($id);
		try{
			$this->orm->building->removeAndFlush($building);
			$this->flashMessage('Buiding was successfully deleted.');
		}catch (NullValueException $e){
			$this->flashMessage('Buiding cant be deleted.', 'warning');
		}

		$this->redirect('Building:default');
	}

	public function renderDefault()
	{
		$this->template->buildings = $this->orm->building->findAll();
	}

	public function actionDetail($id)
	{
		if ($this->orm->building->getById($id) == NULL)
		{
			throw new BadRequestException("Building $id was not found!");
		}
	}

	public function renderDetail($id)
	{
		$this->template->building = $this->orm->building->getById($id);
	}

	public function renderEdit($id)
	{
	}

	public function renderAdd($id)
	{
	}

	/**
	 * @param IBuildingFactory $buildingFactory
	 * @return BuildingControl
	 */
	protected function createComponentBuilding(IBuildingFactory $buildingFactory)
	{
		$building = $buildingFactory->create($this->getParameter('id'));
		$building->onSuccess[] = function ()
		{
			$this->redirect('Building:default');
		};
		return $building;
	}
}
