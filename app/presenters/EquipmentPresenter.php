<?php

namespace App\Presenters;

use App\Components\Equipment\EquipmentControl;
use App\Components\Equipment\IEquipmentFactory;
use Nette\Database\ForeignKeyConstraintViolationException;
use Nextras\Orm\NullValueException;

/**
 * Class EquipmentPresenter
 */
class EquipmentPresenter extends SecuredPresenter
{

	public function handleDelete($id)
	{
		$userEquipment = $this->orm->userEquipment->findBy(['this->user->id' => $this->user->id, 'this->equipment->id'=> $id])->fetch();
        $equipment = $this->orm->equipment->getById($id);
		try{
			$this->orm->userEquipment->removeAndFlush($userEquipment);
            $this->orm->equipment->removeAndFlush($equipment);
			$this->flashMessage("Equipment was successfully deleted.");
		}catch (NullValueException $e) {
			$this->flashMessage("Equipment cant be deleted.", "warning");
		}
		$this->redirect('Equipment:default');
	}

	public function renderDefault()
	{
		$this->template->equipments = $this->orm->equipment->findAll();
	}

	public function renderDetail($id)
	{
		$this->template->equipment = $this->orm->equipment->getById($id);
	}

	public function renderEdit($id)
	{
	}

	public function renderAdd($id)
	{
	}

	/**
	 * @param IEquipmentFactory $equipmentFactory
	 * @return EquipmentControl
	 */
	protected function createComponentEquipment(IEquipmentFactory $equipmentFactory)
	{
		$equipment = $equipmentFactory->create($this->getParameter('id'));
		$equipment->onSuccess[] = function ()
		{
			$this->redirect('Equipment:default');
		};
		return $equipment;
	}
}
