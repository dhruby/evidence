<?php

namespace App\Presenters;

/**
 * Class SecuredPresenter
 */
class SecuredPresenter extends BasePresenter
{

	protected function startup()
	{
		parent::startup();
		// TODO: check user
		if(!$this->user->isLoggedIn())
		{
			$this->redirect('Auth:login');
		}

		if (!$this->user->isAllowed($this->name, $this->view))
		{
			$this->redirect('Dashboard:default');
		}
	}

	public function handleSignOut()
	{
		$this->getUser()->logout();
		$this->redirect('Auth:login');
	}
}