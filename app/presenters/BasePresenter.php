<?php

namespace App\Presenters;

use App\Model;
use Kdyby\Autowired\AutowireComponentFactories;
use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/**
	 * @var Model\Orm
	 * @inject
	 */
	public $orm;

	use AutowireComponentFactories;

}
