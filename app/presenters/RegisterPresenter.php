<?php

namespace App\Presenters;


use App\Components\Registration\IRegistrationFactory;
use App\Components\Registration\RegistrationControl;

class RegisterPresenter extends BasePresenter
{
	/**
	 * @param IRegistrationFactory $registrationFactory
	 * @return RegistrationControl
	 */
	protected function createComponentRegistration(IRegistrationFactory $registrationFactory)
	{
		$registration = $registrationFactory->create();
		$registration->onSuccess[] = function ()
		{

			$this->redirect('Dashboard:default');
		};
		return $registration;
	}
}
