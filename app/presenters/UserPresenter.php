<?php

namespace App\Presenters;

use App\Components\User\IUserFactory;
use App\Components\User\UserControl;
use Nextras\Dbal\ForeignKeyConstraintViolationException;
use Nextras\Orm\NullValueException;

/**
 * Class UserPresenter
 */
class UserPresenter extends SecuredPresenter
{
	public function actionDelete($id)
	{
		$user = $this->orm->user->getById($id);
		try{
			$this->orm->user->removeAndFlush($user);
			$this->flashMessage("User was successfully deleted.");
		}catch(ForeignKeyConstraintViolationException $e){
			$this->flashMessage("User cant be deleted.", "warning");
		}

		$this->redirect('User:default');
	}

	public function renderDefault()
	{
		$this->template->users = $this->orm->user->findAll();
	}

	public function renderEdit($id)
	{

	}

	public function renderAdd($id)
	{

	}

	/**
	 * @param IUserFactory $userFactory
	 * @return UserControl
	 */
	protected function createComponentUser(IUserFactory $userFactory)
	{
		$user = $userFactory->create($this->getParameter('id'));
		$user->onSuccess[] = function ()
		{
			$this->redirect('User:default');
		};
		return $user;
	}
}
