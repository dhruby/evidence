<?php

namespace App\Presenters;

/**
 * Class DashboardPresenter
 */
class DashboardPresenter extends SecuredPresenter
{

	public function renderDefault()
	{
		$this->template->equipments = $this->orm->userEquipment->findBy([
			'this->user->id' => $this->user->id
		]);

		if($this->user->isInRole('technician'))
		{
			$this->template->technician = true;
			$technician = $this->orm->technician->getBy(['user' => $this->user->id]);
			$this->template->repairs = $this->orm->repair->findBy([
				'assignedTo' => $technician->id
			]);
		}
	}

}