<?php

namespace App\Presenters;

use App\Components\Login\ILoginFactory;
use App\Components\Login\LoginControl;

/**
 * Class AuthPresenter
 */
class AuthPresenter extends BasePresenter
{

	/**
	 * @param ILoginFactory $loginFactory
	 * @return LoginControl
	 */
	protected function createComponentLogin(ILoginFactory $loginFactory)
	{
		$login = $loginFactory->create();
		$login->onSuccess[] = function ()
		{
			$this->redirect('Dashboard:default');
		};
		return $login;
	}
}