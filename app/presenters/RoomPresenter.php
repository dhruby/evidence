<?php

namespace App\Presenters;

use App\Components\Room\IRoomFactory;
use App\Components\Room\RoomControl;
use Nette\Application\AbortException;
use Nextras\Orm\NullValueException;

/**
 * Class RoomPresenter
 */
class RoomPresenter extends SecuredPresenter
{
	public function handleDelete($id)
	{
		$room = $this->orm->room->getById($id);
		try{
			$this->orm->room->removeAndFlush($room);
			$this->flashMessage('Room was successfully deleted.');
		}
		catch (NullValueException $e)
		{
			$this->flashMessage('Room cant be deleted.', "warning");
		}

		$this->redirect('Room:default');
	}

	public function renderDefault()
	{
		$this->template->rooms = $this->orm->room->findAll();
	}

	public function renderDetail($id)
	{
		$this->template->room = $this->orm->room->getById($id);
	}

	public function renderEdit($id)
	{

	}

	public function renderAdd($id)
	{

	}

	/**
	 * @param IRoomFactory $roomFactory
	 * @return RoomControl
	 */
	protected function createComponentRoom(IRoomFactory $roomFactory)
	{
		$room = $roomFactory->create($this->getParameter('id'));
		$room->onSuccess[] = function ()
		{
			$this->redirect('Room:default');
		};
		return $room;
	}
}
