<?php

namespace App\Presenters;

use App\Components\Department\DepartmentControl;
use App\Components\Department\IDepartmentFactory;
use Nextras\Orm\NullValueException;

/**
 * Class DepartmentPresenter
 */
class DepartmentPresenter extends SecuredPresenter
{
	public function handleDelete($id)
	{
		$department = $this->orm->department->getById($id);
		try{

			$this->orm->department->removeAndFlush($department);
			$this->flashMessage("Department was successfully deleted.");
		}catch(NullValueException $e){
			$this->flashMessage("Department cant be deleted.", "warning");
		}

		$this->redirect('Department:default');
	}

	public function renderDefault($id = NULL) // @param $id because of edit
	{
		$this->template->departments = $this->orm->department->findAll();
	}

	public function renderDetail($id)
	{
		$this->template->department = $this->orm->department->getById($id);
	}

	public function handleEdit($id)
	{
		$this->template->showEdit = true;
		$this->redrawControl('edit');
	}

	public function renderAdd($id)
	{

	}

	/**
	 * @param IDepartmentFactory $departmentFactory
	 * @return DepartmentControl
	 */
	protected function createComponentDepartment(IDepartmentFactory $departmentFactory)
	{
		$department = $departmentFactory->create($this->getParameter('id'));
		$department->onSuccess[] = function ()
		{
			$this->redirect('Department:default');
		};
		return $department;
	}
}
